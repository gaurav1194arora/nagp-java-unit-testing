package com.nagp.java.unit.testing.integration;

import java.util.Collections;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.nagp.java.unit.testing.EBrokerApplication;

@SpringBootTest(
		webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
		classes = EBrokerApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
public class EbrokerIntegrationTest {

	
	@Autowired
    private MockMvc mockMvc;
	
	
	@Test
	public void applicationContextTest() {
		EBrokerApplication.main(new String[] {});
	}
	
	
	@DisplayName("Test create Trader and add funds and buy stocks")
	@Test
    public void testCreateTraderAddFundAndBuyStock() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.post("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "        \"fund\":10,\n" +
                        "        \"name\":\"Gaurav\"\n" +
                        "}"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is("Gaurav")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.fund", Matchers.is(10.0)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.stocks", Matchers.is(Collections.EMPTY_LIST)));
        
        
        mockMvc.perform(MockMvcRequestBuilders.put("/{userId}/fund", 1).queryParam("fund", "1000"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());

        mockMvc.perform(MockMvcRequestBuilders.post("/{userId}/stock/buy", 1)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                		"        \"price\":100,\n" +
                        "        \"quantity\":5,\n" +
                        "        \"name\":\"TATA\"\n" +
                        "}"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.price", Matchers.is(100.0)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.quantity", Matchers.is(5)))
				.andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is("TATA")));

       
    }

}
