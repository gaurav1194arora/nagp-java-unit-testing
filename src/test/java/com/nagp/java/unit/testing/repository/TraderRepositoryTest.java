package com.nagp.java.unit.testing.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.nagp.java.unit.testing.models.Trader;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class TraderRepositoryTest {

	@Autowired
	private TraderRepository traderRepository;

	@DisplayName("Test Create, read, delete operations for Trader Repository")
	@Test
	public void testCreateReadDelete() {

		Trader trader = new Trader();
		trader.setId(1);
		trader.setName("Gaurav");
		trader.setFund(1000.0d);

		trader = traderRepository.save(trader);

		Optional<Trader> t = traderRepository.findById(trader.getId());

		assertThat(t.get().getName()).isEqualTo("Gaurav");

		traderRepository.delete(t.get());

		Assertions.assertThat(traderRepository.findAll()).isEmpty();

	}
}
