package com.nagp.java.unit.testing.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import com.nagp.java.unit.testing.exceptions.InsufficientFundException;
import com.nagp.java.unit.testing.exceptions.StockNotFoundException;
import com.nagp.java.unit.testing.exceptions.TraderNotFoundException;
import com.nagp.java.unit.testing.models.Stock;
import com.nagp.java.unit.testing.models.Trader;
import com.nagp.java.unit.testing.repository.StockRepository;
import com.nagp.java.unit.testing.repository.TraderRepository;
import com.nagp.java.unit.testing.service.impl.StockServiceImpl;
import com.nagp.java.unit.testing.utils.Utility;

@ExtendWith(MockitoExtension.class)
public class StockServiceTest {

	@InjectMocks
	private StockServiceImpl stockService;

	@Mock
	private StockRepository stockRepository;

	@Mock
	private TraderRepository traderRepository;

	@BeforeEach
	public void init() {
		MockitoAnnotations.openMocks(this);

	}

	@DisplayName("Should not buy stock when not placed in period ")
	@Test
	public void shouldNotBuyStockWhenNotPlacedInPeriod() {

		Stock stock = new Stock();
		stock.setPrice(100);
		stock.setQuantity(5);
		stock.setName("TATA");

		try (MockedStatic<Utility> utilites = Mockito.mockStatic(Utility.class)) {
			utilites.when(Utility::checkIfActionTakenInPeriod).thenReturn(false);
			stockService.buyStock(1, stock);
		}

		verify(traderRepository, times(0)).findById((long) 1);
		verify(stockRepository, times(0)).save(stock);

	}

	@DisplayName("Should not sell stock when not placed in period ")
	@Test
	public void shouldNotSellStockWhenNotPlacedInPeriod() {

		Stock stock = new Stock();
		stock.setPrice(100);
		stock.setQuantity(5);
		stock.setName("TATA");

		try (MockedStatic<Utility> utilites = Mockito.mockStatic(Utility.class)) {
			utilites.when(Utility::checkIfActionTakenInPeriod).thenReturn(false);
			stockService.sellStock(1, stock);
		}

		verify(traderRepository, times(0)).findById((long) 1);
		verify(stockRepository, times(0)).save(stock);

	}

	@DisplayName("Should throw exception when buying stock with no trader exist")
	@Test
	public void shouldThrowExceptionWhenBuyStockWithTraderNotExist() {

		Stock stock = new Stock();
		stock.setId(1);
		stock.setPrice(100);
		stock.setQuantity(5);
		stock.setName("TATA");

		Optional<Trader> trader = Optional.ofNullable(null);

		when(traderRepository.findById((long) 1)).thenReturn(trader);

		try (MockedStatic<Utility> utilites = Mockito.mockStatic(Utility.class)) {
			utilites.when(Utility::checkIfActionTakenInPeriod).thenReturn(true);
			assertThrows(TraderNotFoundException.class, () -> stockService.buyStock(1, stock));
		}

		verify(traderRepository, times(1)).findById((long) 1);
		verify(stockRepository, times(0)).save(stock);

	}

	@DisplayName("Should able to buy stock when trader exist with sufficient funds")
	@Test
	public void shouldBuyStockWhenTraderExistWithSufficientFund() {

		Stock stock = new Stock();
		stock.setId(1);
		stock.setPrice(100);
		stock.setQuantity(5);
		stock.setName("TATA");

		Trader t = new Trader();
		t.setId(1);
		t.setName("Gaurav");
		t.setFund(1000.0d);
		t.setStocks(new ArrayList<>());

		Optional<Trader> trader = Optional.of(t);

		when(traderRepository.findById((long) 1)).thenReturn(trader);
		when(traderRepository.save(trader.get())).thenReturn(trader.get());
		when(stockRepository.save(stock)).thenReturn(stock);

		try (MockedStatic<Utility> utilites = Mockito.mockStatic(Utility.class)) {
			utilites.when(Utility::checkIfActionTakenInPeriod).thenReturn(true);
			utilites.when(() -> Utility.checkIfSuficientFund(t.getFund(), stock.getPrice(), stock.getQuantity()))
					.thenReturn(true);
			Stock addedStock = stockService.buyStock(1, stock);
			assertEquals(addedStock.getQuantity(), stock.getQuantity());
		}

		assertEquals(1, t.getStocks().size());
		assertEquals(t.getFund(), 500.0);
		verify(traderRepository, times(1)).findById((long) 1);
		verify(traderRepository, times(1)).save(trader.get());
		verify(stockRepository, times(1)).save(stock);

	}

	@DisplayName("Should throw exception while stock buy when trader exist with insufficient funds")
	@Test
	public void shouldThrowExceptionWhileStockBuyWhenTraderExistWithInSufficientFund() {

		Stock stock = new Stock();
		stock.setId(1);
		stock.setPrice(100);
		stock.setQuantity(5);
		stock.setName("TATA");

		Trader t = new Trader();
		t.setId(1);
		t.setName("Gaurav");
		t.setFund(100.0d);
		t.setStocks(new ArrayList<>());

		Optional<Trader> trader = Optional.of(t);

		when(traderRepository.findById((long) 1)).thenReturn(trader);

		try (MockedStatic<Utility> utilites = Mockito.mockStatic(Utility.class)) {
			utilites.when(Utility::checkIfActionTakenInPeriod).thenReturn(true);
			assertThrows(InsufficientFundException.class, () -> stockService.buyStock(1, stock));
		}

		verify(traderRepository, times(1)).findById((long) 1);
		verify(traderRepository, times(0)).save(trader.get());
		verify(stockRepository, times(0)).save(stock);

	}

	@DisplayName("Should throw exception while selling stock and trader not exist")
	@Test
	public void shouldThrowExceptionWhileStockSellWhenTraderNotExist() {

		Stock stock = new Stock();
		stock.setId(1);
		stock.setPrice(100);
		stock.setQuantity(5);
		stock.setName("TATA");

		Optional<Trader> trader = Optional.ofNullable(null);

		when(traderRepository.findById((long) 1)).thenReturn(trader);

		try (MockedStatic<Utility> utilites = Mockito.mockStatic(Utility.class)) {
			utilites.when(Utility::checkIfActionTakenInPeriod).thenReturn(true);
			assertThrows(TraderNotFoundException.class, () -> stockService.sellStock(1, stock));
		}

		verify(traderRepository, times(1)).findById((long) 1);
		verify(stockRepository, times(0)).save(stock);

	}

	@DisplayName("Should throw exception while selling stock and trader do not have stock")
	@Test
	public void shouldThrowExceptionWhileStockSellWhenTraderNotHaveStock() {

		Stock stock = new Stock();
		stock.setId(1);
		stock.setPrice(100);
		stock.setQuantity(5);
		stock.setName("TATA");

		Trader t = new Trader();
		t.setId(1);
		t.setName("Gaurav");
		t.setFund(1000.0d);
		t.setStocks(new ArrayList<>());

		Optional<Trader> trader = Optional.of(t);

		when(traderRepository.findById((long) 1)).thenReturn(trader);

		try (MockedStatic<Utility> utilites = Mockito.mockStatic(Utility.class)) {
			utilites.when(Utility::checkIfActionTakenInPeriod).thenReturn(true);
			assertThrows(StockNotFoundException.class, () -> stockService.sellStock(1, stock));
		}

		verify(traderRepository, times(1)).findById((long) 1);
		verify(stockRepository, times(0)).save(stock);

	}

	@DisplayName("Should sell stock when trader exist")
	@Test
	public void shouldSellStockWhenTraderExist() {

		Stock stock = new Stock();
		stock.setId(1);
		stock.setPrice(100);
		stock.setQuantity(5);
		stock.setName("TATA");

		Trader t = new Trader();
		t.setId(1);
		t.setName("Gaurav");
		t.setFund(1000.0d);

		List<Stock> stocks = new ArrayList<>();
		stocks.add(stock);
		t.setStocks(stocks);

		Optional<Trader> trader = Optional.of(t);

		when(traderRepository.findById((long) 1)).thenReturn(trader);
		when(traderRepository.save(trader.get())).thenReturn(trader.get());

		try (MockedStatic<Utility> utilites = Mockito.mockStatic(Utility.class)) {
			utilites.when(Utility::checkIfActionTakenInPeriod).thenReturn(true);
			Stock soldStock = stockService.sellStock(1, stock);
			assertEquals(soldStock.getId(), stock.getId());
			assertEquals(soldStock.getQuantity(), stock.getQuantity());
		}

		assertEquals(0, t.getStocks().size());
		assertEquals(t.getFund(), 1500.0);
		verify(traderRepository, times(1)).findById((long) 1);
		verify(traderRepository, times(1)).save(trader.get());
		verify(stockRepository, times(1)).delete(stock);

	}

}
