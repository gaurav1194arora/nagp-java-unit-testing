package com.nagp.java.unit.testing.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.nagp.java.unit.testing.models.Stock;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class StockRepositoryTest {

	@Autowired
	private StockRepository stockRepository;

	@DisplayName("Test Create, read, delete operations for Stock repository")
	@Test
	public void testCreateReadDelete() {

		Stock stock = new Stock();
		stock.setPrice(100);
		stock.setQuantity(5);
		stock.setName("TATA");

		stockRepository.save(stock);

		List<Stock> stocks = stockRepository.findAll();

		assertThat(stocks).extracting((s) -> s.getName()).containsOnly("TATA");

		stockRepository.delete(stock);

		Assertions.assertThat(stockRepository.findAll()).isEmpty();

	}
}
