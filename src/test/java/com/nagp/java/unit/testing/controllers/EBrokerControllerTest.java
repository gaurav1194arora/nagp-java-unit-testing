package com.nagp.java.unit.testing.controllers;

import static org.mockito.Mockito.doNothing;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.nagp.java.unit.testing.controller.EbrokerController;
import com.nagp.java.unit.testing.models.Stock;
import com.nagp.java.unit.testing.models.Trader;
import com.nagp.java.unit.testing.service.StockService;
import com.nagp.java.unit.testing.service.TraderService;

@ExtendWith(SpringExtension.class)
@WebMvcTest(EbrokerController.class)
public class EBrokerControllerTest {

	@MockBean
	private StockService stockService;

	@MockBean
	private TraderService traderService;

	@Autowired
	private MockMvc mockMvc;

	@DisplayName("Test create Trader")
	@Test
	public void testCreateTrader() throws Exception {

		Trader trader = new Trader();
		trader.setName("Gaurav");
		trader.setFund(0.0d);

		Mockito.when(traderService.create(trader)).thenReturn(trader);

		mockMvc.perform(MockMvcRequestBuilders.post("/user").
				contentType(MediaType.APPLICATION_JSON)
				.content("{\n" +
                        "        \"fund\":0,\n" +
                        "        \"name\":\"Gaurav\"\n" +
                        "}"))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is("Gaurav")))
				.andExpect(MockMvcResultMatchers.jsonPath("$.fund", Matchers.is(0.0)));
	}

	@DisplayName("Test buy stock")
	@Test
	public void testBuyStock() throws Exception {
		
		Stock stock = new Stock();
		stock.setId(1);
		stock.setPrice(100);
		stock.setQuantity(5);
		stock.setName("TATA");
		
		Mockito.when(stockService.buyStock(1, stock)).thenReturn(stock);
		
		mockMvc.perform(MockMvcRequestBuilders.post("/{userId}/stock/buy", 1)
	                .contentType(MediaType.APPLICATION_JSON)
	                .content("{\n" +
	                		"        \"id\":1,\n" +
	                		"        \"price\":100,\n" +
	                        "        \"quantity\":5,\n" +
	                        "        \"name\":\"TATA\"\n" +
	                        "}"))
	                .andExpect(MockMvcResultMatchers.status().isOk())
	                .andDo(MockMvcResultHandlers.print())
	                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(1)))
	                .andExpect(MockMvcResultMatchers.jsonPath("$.price", Matchers.is(100.0)))
	                .andExpect(MockMvcResultMatchers.jsonPath("$.quantity", Matchers.is(5)))
					.andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is("TATA")));

		
	}
	
	@DisplayName("Test sell stock")
	@Test
	public void testSellStock() throws Exception {
		
		Stock oldStock = new Stock();
		oldStock.setId(1);
		oldStock.setPrice(100);
		oldStock.setQuantity(5);
		oldStock.setName("TATA");
		
		Stock newStock = new Stock();
		newStock.setId(1);
		newStock.setPrice(100);
		newStock.setQuantity(0);
		newStock.setName("TATA");
		
		
		
		Mockito.when(stockService.sellStock(1, oldStock)).thenReturn(newStock);
		
		mockMvc.perform(MockMvcRequestBuilders.post("/{userId}/stock/sell", 1)
	                .contentType(MediaType.APPLICATION_JSON)
	                .content("{\n" +
	                		"        \"id\":1,\n" +
	                		"        \"price\":100,\n" +
	                        "        \"quantity\":5,\n" +
	                        "        \"name\":\"TATA\"\n" +
	                        "}"))
	                .andExpect(MockMvcResultMatchers.status().isOk())
	                .andDo(MockMvcResultHandlers.print())
	                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(1)))
	                .andExpect(MockMvcResultMatchers.jsonPath("$.price", Matchers.is(100.0)))
	                .andExpect(MockMvcResultMatchers.jsonPath("$.quantity", Matchers.is(0)))
					.andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is("TATA")));

		
	}
	
	@DisplayName("test add fund")
	@Test
	public void testaddFund() throws Exception {

		doNothing().when(traderService).addFund(100, 1);

		mockMvc.perform(MockMvcRequestBuilders.put("/{userId}/fund", 1).queryParam("fund", "100"))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andDo(MockMvcResultHandlers.print());
				
	}
}
