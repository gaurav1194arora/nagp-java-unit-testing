package com.nagp.java.unit.testing.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import com.nagp.java.unit.testing.exceptions.TraderNotFoundException;
import com.nagp.java.unit.testing.models.Trader;
import com.nagp.java.unit.testing.repository.TraderRepository;
import com.nagp.java.unit.testing.service.impl.TraderServiceImpl;

@ExtendWith(MockitoExtension.class)
public class TraderServiceTest {

	@InjectMocks
	private TraderServiceImpl traderService;

	@Mock
	private TraderRepository traderRepository;

	@BeforeEach
	public void init() {
		MockitoAnnotations.openMocks(this);
	}

	@DisplayName("Should create trader")
	@Test
	public void shouldCreateTrader() {

		Trader trader = new Trader();
		trader.setId(1);
		trader.setName("Gaurav");
		trader.setFund(1000.0d);

		when(traderRepository.save(trader)).thenReturn(trader);

		Trader createdTrader = traderService.create(trader);

		assertEquals(createdTrader.getId(), trader.getId());
		assertEquals(Arrays.asList(), trader.getStocks());
		verify(traderRepository, times(1)).save(trader);
	}

	@DisplayName("Should be able to add fund")
	@Test
	public void shouldAbleToAddFund() {

		Trader t = new Trader();
		t.setId(1);
		t.setName("Gaurav");
		t.setFund(1000.0d);

		double fund = 1000.0d;

		Optional<Trader> trader = Optional.of(t);

		when(traderRepository.findById((long) 1)).thenReturn(trader);
		when(traderRepository.save(trader.get())).thenReturn(trader.get());

		traderService.addFund(fund, 1);

		assertEquals(2000.0, t.getFund());
		verify(traderRepository, times(1)).save(trader.get());
	}

	@DisplayName("Should throw exception when add fund with no trader exist")
	@Test
	public void shouldThrowExceptionWhenAddFundWithTraderNotExist() {

		double fund = 1000.0d;

		Optional<Trader> trader = Optional.ofNullable(null);

		when(traderRepository.findById((long) 1)).thenReturn(trader);

		assertThrows(TraderNotFoundException.class, () -> traderService.addFund(fund, 1));
		verify(traderRepository, times(1)).findById((long) 1);

	}
}
