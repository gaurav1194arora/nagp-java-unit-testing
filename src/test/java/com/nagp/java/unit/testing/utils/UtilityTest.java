package com.nagp.java.unit.testing.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

public class UtilityTest {

	@DisplayName("Should check for sufficient Fund")
	@Test
	public void ShouldCheckForSufficientFund() {

		double balanceFund = 1000.0d;
		double stockPrice = 100.0d;
		int stockQuantity = 5;

		assertEquals(true, Utility.checkIfSuficientFund(balanceFund, stockPrice, stockQuantity));

		try (MockedStatic<Utility> utilites = Mockito.mockStatic(Utility.class)) {
			utilites.when(() -> Utility.checkIfSuficientFund(balanceFund, stockPrice, stockQuantity)).thenReturn(false);
			assertEquals(false, Utility.checkIfSuficientFund(balanceFund, stockPrice, stockQuantity));
		}

		double balanceFund1 = 100.0d;
		double stockPrice1 = 100.0d;
		int stockQuantity1 = 5;

		assertEquals(false, Utility.checkIfSuficientFund(balanceFund1, stockPrice1, stockQuantity1));
	}

	@DisplayName("Should check if action taken in period")
	@Test
	public void ShouldCheckIfActionTakenInPeriod() {

		assertEquals(false, Utility.checkIfActionTakenInPeriod());

		try (MockedStatic<Utility> utilites = Mockito.mockStatic(Utility.class)) {
			utilites.when(Utility::checkIfActionTakenInPeriod).thenReturn(true);
			assertEquals(true, Utility.checkIfActionTakenInPeriod());
		}

		assertEquals(false, Utility.checkIfActionTakenInPeriod());
	}

}
