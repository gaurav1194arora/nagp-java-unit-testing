package com.nagp.java.unit.testing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EBrokerApplication {

	public static void main(String[] args) {
		SpringApplication.run(EBrokerApplication.class, args);
	}

}
