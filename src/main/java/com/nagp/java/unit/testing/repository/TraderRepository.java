package com.nagp.java.unit.testing.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nagp.java.unit.testing.models.Trader;

@Repository
public interface TraderRepository extends JpaRepository<Trader, Long> {

}
