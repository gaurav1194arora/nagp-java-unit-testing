package com.nagp.java.unit.testing.exceptions;

public class InsufficientFundException extends RuntimeException {

	private static final long serialVersionUID = 4965757610357951862L;

	public InsufficientFundException(String message) {

		super(message);
	}
}
