package com.nagp.java.unit.testing.service.impl;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nagp.java.unit.testing.exceptions.TraderNotFoundException;
import com.nagp.java.unit.testing.models.Trader;
import com.nagp.java.unit.testing.repository.TraderRepository;
import com.nagp.java.unit.testing.service.TraderService;

@Service
public class TraderServiceImpl implements TraderService {

	@Autowired
	private TraderRepository traderRepository;

	@Override
	public void addFund(double fund, long userId) {

		Optional<Trader> t = traderRepository.findById(userId);

		if (t.isPresent()) {

			Trader trader = t.get();

			trader.setFund(trader.getFund() + fund);
			traderRepository.save(trader);

		} else {
			throw new TraderNotFoundException("Trader not found with userId : " + userId);
		}

	}

	@Override
	public Trader create(Trader trader) {

		trader.setStocks(new ArrayList<>());
		return traderRepository.save(trader);
	}

}
