package com.nagp.java.unit.testing.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nagp.java.unit.testing.exceptions.InsufficientFundException;
import com.nagp.java.unit.testing.exceptions.StockNotFoundException;
import com.nagp.java.unit.testing.exceptions.TraderNotFoundException;
import com.nagp.java.unit.testing.models.Stock;
import com.nagp.java.unit.testing.models.Trader;
import com.nagp.java.unit.testing.repository.StockRepository;
import com.nagp.java.unit.testing.repository.TraderRepository;
import com.nagp.java.unit.testing.service.StockService;
import com.nagp.java.unit.testing.utils.Utility;

@Service
public class StockServiceImpl implements StockService {

	@Autowired
	private StockRepository stockRepository;

	@Autowired
	private TraderRepository traderRepository;

	@Override
	public Stock buyStock(long userId, Stock stock) {

		if (Utility.checkIfActionTakenInPeriod()) {

			Optional<Trader> trader = traderRepository.findById(userId);

			if (trader.isPresent()) {

				Trader t = trader.get();
				boolean haveSufficientFund = Utility.checkIfSuficientFund(t.getFund(), stock.getPrice(),
						stock.getQuantity());

				if (haveSufficientFund) {
					stock.setTrader(t);
					t.setFund(t.getFund() - (stock.getQuantity() * stock.getPrice()));
					t.getStocks().add(stock);
					stock = stockRepository.save(stock);
					t = traderRepository.save(t);

				} else {

					throw new InsufficientFundException("Insufficient fund to buy the stock");
				}

			} else {
				throw new TraderNotFoundException("No trader found with the given user Id : " + userId);
			}

		}
		return stock;

	}

	@Override
	public Stock sellStock(long userId, Stock stock) {

		if (Utility.checkIfActionTakenInPeriod()) {
			
			Optional<Trader> trader = traderRepository.findById(userId);

			if (trader.isPresent()) {

				Trader t = trader.get();

				boolean isStockPresent = t.getStocks().stream().anyMatch(s -> s.getName().equals(stock.getName()));

				if (isStockPresent) {

					t.setFund(t.getFund() + (stock.getQuantity() * stock.getPrice()));
					t.getStocks().remove(stock);
					stockRepository.delete(stock);
					traderRepository.save(t);
				} else {
					throw new StockNotFoundException(
							"Stock : " + stock.getName() + " not present for user : " + userId);
				}

			} else {
				throw new TraderNotFoundException("No trader found with the given user Id : " + userId);
			}

		}

		return stock;

	}

}
