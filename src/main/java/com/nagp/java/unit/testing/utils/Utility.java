package com.nagp.java.unit.testing.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Utility {

	public static boolean checkIfSuficientFund(double balanceFund, double stockPrice, int stockQuantity) {

		if (balanceFund >= (stockPrice * stockQuantity)) {
			return true;
		} else {
			return false;
		}

	}

	public static boolean checkIfActionTakenInPeriod() {

		boolean isValidTime = false;

		Calendar currentDate = Calendar.getInstance();
		int dow = currentDate.get(Calendar.DAY_OF_WEEK);
		boolean isValidDay = ((dow >= Calendar.MONDAY) && (dow <= Calendar.FRIDAY));

		try {

			Date startTime = new SimpleDateFormat("HH:mm:ss").parse("09:00:00");
			Calendar calendar1 = Calendar.getInstance();
			calendar1.setTime(startTime);

			Date endTime = new SimpleDateFormat("HH:mm:ss").parse("17:00:00");
			Calendar calendar2 = Calendar.getInstance();
			calendar2.setTime(endTime);

			Date currentTime = currentDate.getTime();
			if (currentTime.after(calendar1.getTime()) && currentTime.before(calendar2.getTime())) {
				isValidTime = true;
			}

		} catch (Exception e) {
		}

		return isValidDay && isValidTime;
	}
}
