package com.nagp.java.unit.testing.service;

import com.nagp.java.unit.testing.models.Trader;

public interface TraderService {

	public void addFund(double fund, long userId);
	
	public Trader create(Trader trader);
}
