package com.nagp.java.unit.testing.models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Table
@Entity
@EqualsAndHashCode
@Getter
@Setter
@NoArgsConstructor
public class Trader {

	@Id
	@GeneratedValue
	private long id;
	private String name;
	private double fund;

	@OneToMany(mappedBy = "trader")
	private List<Stock> stocks;
}
