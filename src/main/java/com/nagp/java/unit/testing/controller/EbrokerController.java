package com.nagp.java.unit.testing.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nagp.java.unit.testing.models.Stock;
import com.nagp.java.unit.testing.models.Trader;
import com.nagp.java.unit.testing.service.StockService;
import com.nagp.java.unit.testing.service.TraderService;

@RestController
public class EbrokerController {

	@Autowired
	private StockService stockService;

	@Autowired
	private TraderService traderService;
	
	@PostMapping("/user")
	public Trader createTrader(@RequestBody Trader trader) {
		
		return traderService.create(trader);
	}

	@PostMapping("/{userId}/stock/buy")
	public Stock buyStock(@RequestBody Stock stock, @PathVariable long userId) {

		return stockService.buyStock(userId, stock);
	}

	@PostMapping("/{userId}/stock/sell")
	public Stock sellStock(@RequestBody Stock stock, @PathVariable long userId) {
		return stockService.sellStock(userId, stock);
	}

	@PutMapping("/{userId}/fund")
	public void addFund(@RequestParam double fund, @PathVariable long userId) {

		traderService.addFund(fund, userId);
	}
}
