package com.nagp.java.unit.testing.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@EqualsAndHashCode
@Getter
@Setter
@NoArgsConstructor
public class Stock {

	@Id
	@GeneratedValue
	private long id;
	private String name;
	private int quantity;
	private double price;

	@ManyToOne
	@JoinColumn(name = "trader_id")
	private Trader trader;
}
