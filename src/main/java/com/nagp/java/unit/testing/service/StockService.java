package com.nagp.java.unit.testing.service;

import com.nagp.java.unit.testing.models.Stock;

public interface StockService {

	public Stock buyStock(long userId, Stock stock);

	public Stock sellStock(long userId, Stock stock);
}
