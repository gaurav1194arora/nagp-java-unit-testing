package com.nagp.java.unit.testing.exceptions;

public class TraderNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public TraderNotFoundException(String message) {

		super(message);
	}
}
