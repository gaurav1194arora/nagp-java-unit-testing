# NAGP Java Unit Testing

1.The Application name is Ebroker
2.Have placed Jacoco report screenshot in jacoco folder. Report can also be found at path tartget/site/jacoco.
3.Before executing test case make sure to have ebroker_test database in your machine.
4.Spring boot main class coverage is excluded in jacoco report as it decreases peformance.
5.Repository layer is not shown in jacoco report as it is an interface extending JPARepository interface.
6.Separate test classes are written for each layer.

